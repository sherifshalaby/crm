<?php

namespace CrmBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CrmBundle\Entity\Label;
use CrmBundle\Entity\Tasks;
use CrmBundle\Form\Type\TasksType;
use Symfony\Component\HttpFoundation\Request;

class DashboardController extends Controller {

    public function indexAction() {

        $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();
        $tasksobject = new Tasks();
        $tasksform = $this->createForm(new TasksType(), $tasksobject);


        $tasks = $em->getRepository('CrmBundle:Tasks')->findBy(array('user' => $user, 'active' => 1), array('notify' => 'ASC'));

        return $this->render('CrmBundle:Dashboard:index.html.twig', array('tasks' => $tasks, 'form' => $tasksform->createView(),));
    }

    public function tasksAction() {

        $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $taskobject = new Tasks();
        $form = $this->createForm(new TasksType(), $taskobject);

        $request = $this->getRequest();

        if ($request->getMethod() == 'POST') {
            $user = $em->getRepository('CrmBundle:User')->find($user);
            $taskobject->setUser($user);
            $form->bind($request);

            if ($form->isValid()) {

                $em->persist($taskobject);
                $em->flush();

                $this->get('session')->getFlashBag()->add('success', 'New Task Added.');
            } else {
                $this->get('session')->getFlashBag()->add('error', 'Failed to add task.');
            }
        }

        return $this->redirect($this->generateUrl('crm_dashboard'));
    }

    public function checktaskAction(Tasks $task) {

        $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();
        $all = $em->getRepository('CrmBundle:Tasks')->findAll();

        foreach($all as $one)
        {
            if($task == $one)
            {
                break;
            }
        }
        
        // $task = $em->getRepository('CrmBundle:Tasks')->find($id);

        $one->setActive(0);
        $em->persist($one);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Task Checked.');
        return $this->redirect($this->generateUrl('crm_dashboard'));
    }

    public function productimportAction(Request $files) {


        $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

      
        $em = $this->getDoctrine()->getManager();
        $uploadDir = $this->get('kernel')->getRootDir() . '/../web/csv/';
        $batch = 1;
        if (!is_dir($uploadDir)) {
            mkdir($uploadDir, 0777);
        }
        $temp = $files->files->get('products')->getRealPath();
        move_uploaded_file($temp, "$uploadDir" . $batch . ".csv");
        $rowIndex = 0;





        if (($handle = fopen($uploadDir . $batch . ".csv", "r")) !== FALSE) {

            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                // Check missing field in row file 
                if ($rowIndex != 0) {
                    $label = $em->getRepository('CrmBundle:Label')->findBy(array('name' => $data[0], 'type' => $data[1]));

                    if ($label) {
                        
                    } else {
                        $labelobject = new Label();
                        $labelobject->setName($data[0]);
                        $labelobject->setType($data[1]);
                        $em->persist($labelobject);
                        $flagforflush = 1;
                    }
                    //     echo "<pre>"; \Doctrine\Common\Util\Debug::dump($label);   exit;
                }

                $rowIndex++;
            }

            if ($flagforflush == 1) {
                $em->flush();
            }

            $this->get('session')->getFlashBag()->add('success', 'Products Imported Successfuly');
        }



        return $this->redirect($this->generateUrl('crm_dashboard'));
    }

}
