<?php

namespace CrmBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CrmBundle\Form\Type\LabelType;
use CrmBundle\Entity\Label;
use CrmBundle\Entity\Leads;
use CrmBundle\Entity\LeadsLabels;
use CrmBundle\Form\Type\LeadsLabelsType;


class LabelController extends Controller {

    public function indexAction() {
        
          $user = $this->container->get('security.context')->getToken()->getUser();
        if($user === "anon." )
       {
           return $this->redirect($this->generateUrl('fos_user_security_login'));
       }
        
        $em = $this->getDoctrine()->getManager();
     
        $labelobject = new Label();
       
        $form = $this->createForm(new LabelType(), $labelobject);

           $request = $this->getRequest();
        
            if ($request->getMethod() == 'POST') {

            $form->bind($request);
            if ($form->isValid()) {
            $em->persist($labelobject);
            $em->flush();
                   $labelobject = new Label();
       
        $form = $this->createForm(new LabelType(), $labelobject);     
      $this->get('session')->getFlashBag()->add('success', 'Label Added.');
            }
        }
           $labels = $em->getRepository('CrmBundle:Label')->findAll();
          return $this->render('CrmBundle:Label:index.html.twig', array('labels' => $labels,'labelform' => $form->createView()));
    }

    public function deleteAction(Label $label) {
        
          $user = $this->container->get('security.context')->getToken()->getUser();
        if($user === "anon." )
       {
           return $this->redirect($this->generateUrl('fos_user_security_login'));
       }
        
 
                         try {
       $em = $this->getDoctrine()->getManager();
     //   $object = $em->getRepository('CrmBundle:Label')->find($id);
        $em->remove($label);
        $em->flush();
         $this->get('session')->getFlashBag()->add('success', 'Label Deleted.');
    }
    catch (\Exception $e) {
             $this->get('session')->getFlashBag()->add('error', 'There are Leads inetersted in this Unit. as an alternative update Unit name if you should.');
    }
       
  
        return $this->redirect($this->generateUrl('crm_label'));
    }

    public function editAction(Label $label) {
        
          $user = $this->container->get('security.context')->getToken()->getUser();
        if($user === "anon." )
       {
           return $this->redirect($this->generateUrl('fos_user_security_login'));
       }
        
        $em = $this->getDoctrine()->getManager();
        //$labelobject = $em->getRepository('CrmBundle:Label')->find($id);
        
                $form = $this->createForm(new LabelType(), $label);

           $request = $this->getRequest();
        
            if ($request->getMethod() == 'POST') {

            $form->bind($request);
            if ($form->isValid()) {
            $em->persist($label);
            $em->flush();
  $this->get('session')->getFlashBag()->add('success', 'Label Updated.');
    
            }
        }

               return $this->render('CrmBundle:Label:editlabel.html.twig', array('labelform' => $form->createView(),'id' => $label->getId()));
    }

    public function deletelabelAction(LeadsLabels $leadlabel,  Leads $lead) {

        $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();
       // $object = $em->getRepository('CrmBundle:LeadsLabels')->find($lid);
        $em->remove($leadlabel);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Label Deleted From Lead Successfuly.');
        return $this->redirect($this->generateUrl('crm_history', array('lead' => $lead->getId())));
    }
    
    
    public function addlabelAction(Leads $lead) {

        $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();
      //  $leadobject = $em->getRepository('CrmBundle:Leads')->find($id);

        $leadlabels = new LeadsLabels();
        $labelform = $this->createForm(new LeadsLabelsType(), $leadlabels);

        $request = $this->getRequest();

        if ($request->getMethod() == 'POST') {

            $labelform->bind($request);

            //    echo "<pre>";Debug::dump($labelform);exit;
            if ($labelform->isValid()) {

                $leadlabels->setLead($lead);
                $em->persist($leadlabels);
                $em->flush();
                $this->get('session')->getFlashBag()->add('success', 'New Label Added.');
            }
        }

        return $this->redirect($this->generateUrl('crm_history', array('lead' => $lead->getId())));
    }


}
