<?php

namespace CrmBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CrmBundle\Form\Type\LeadsType;
use CrmBundle\Entity\Leads;
use CrmBundle\Form\Type\LeadsLabelsType;
use CrmBundle\Entity\LeadsLabels;
use CrmBundle\Entity\LeadsUsers;
use CrmBundle\Form\Type\LabelType;
use CrmBundle\Entity\Label;

use CrmBundle\Form\Type\ActivityType;
use CrmBundle\Entity\Activity;
use CrmBundle\Entity\Tasks;
use CrmBundle\Form\Type\userTasksType;
use CrmBundle\Form\Type\LogType;
use CrmBundle\Entity\Log;
use Symfony\Component\HttpFoundation\Request;
class LeadsController extends Controller {

    public function indexAction() {

        
          $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();

        $leadobject = new Leads();
        $form = $this->createForm(new LeadsType(), $leadobject);

        $labelobject = new Label();
        $labelform = $this->createForm(new LabelType(), $labelobject);


        $request = $this->getRequest();

        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            if ($form->isValid()) {
                $leadobject->setUser($user);
                $em->persist($leadobject);
                $em->flush();
             
                // $leadobject = new Leads();
                //     $form = $this->createForm(new LeadsType(), $leadobject);     
                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('New Lead has been Saved.'));
                return $this->redirect($this->generateUrl('crm_history', array('lead' => $leadobject->getId())));
            }
        }


        $sharedleadsobject = $em->getRepository('CrmBundle:LeadsUsers')->findBy(array('user' => $user), array('updated' => 'ASC'));
   
       $i=0;
        foreach($sharedleadsobject as $sharedlead)
        {
          $sharedleads[$i] =  $sharedlead->getLead();
$i++;
        }
             
        $leads = $em->getRepository('CrmBundle:Leads')->findBy(array('user' => $user), array('updated' => 'ASC'));
        
    if($i>0)
    {
           return $this->render('CrmBundle:Leads:index.html.twig', array('sharedleads'=>$sharedleads,'activityform' => $activityform->createView(), 'leads' => $leads, 'form' => $form->createView(), 'labelform' => $labelform->createView()));
      
    }  else {
              return $this->render('CrmBundle:Leads:index.html.twig', array( 'leads' => $leads, 'form' => $form->createView(), 'labelform' => $labelform->createView()));
   
    }
   }

    public function allleadsAction() {

        $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $leadobject = new Leads();
        $form = $this->createForm(new LeadsType(), $leadobject);

        $labelobject = new Label();
        $labelform = $this->createForm(new LabelType(), $labelobject);

        $activityobject = new Activity();
        $activityform = $this->createForm(new ActivityType(), $activityobject);

        $request = $this->getRequest();

        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            if ($form->isValid()) {
                $leadobject->setUser($user);
                $em->persist($leadobject);
                $em->flush();
         
                // $leadobject = new Leads();
                //     $form = $this->createForm(new LeadsType(), $leadobject);     
                $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('New Lead has been Saved.'));
                return $this->redirect($this->generateUrl('crm_history', array('lead' => $leadobject->getId())));
            }
        }



        $leads = $em->getRepository('CrmBundle:Leads')->findAll();

        return $this->render('CrmBundle:Leads:index.html.twig', array('activityform' => $activityform->createView(), 'leads' => $leads, 'form' => $form->createView(), 'labelform' => $labelform->createView()));
    }

    public function deleteAction(Leads $lead) {

        $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }


        $em = $this->getDoctrine()->getManager();
         $all = $em->getRepository('CrmBundle:Leads')->findAll();

        foreach($all as $one)
        {
            if($lead == $one)
            {
                break;
            }
        }
      //  $object = $em->getRepository('CrmBundle:Leads')->find($id);
        $em->remove($one);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'Lead Deleted.');
        return $this->redirect($this->generateUrl('crm_leads'));
    }



    public function editAction(Leads $lead) {


        $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();
    //    $leadobject = $em->getRepository('CrmBundle:Leads')->find($id);

        $leadform = $this->createForm(new LeadsType(), $lead);

        $request = $this->getRequest();

        if ($request->getMethod() == 'POST') {
            $leadform->bind($request);


            if ($leadform->isValid()) {

                $em->persist($lead);
                $em->flush();

                $this->get('session')->getFlashBag()->add('success', 'Lead Updated.');
            }
        }


        return $this->redirect($this->generateUrl('crm_history', array('lead' => $lead->getId())));
    }

    public function historyAction(Leads $lead) {

        $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();
      //  $leadobject = $em->getRepository('CrmBundle:Leads')->find($id);

   
        $tasksobject = new Tasks();
        $tasksform = $this->createForm(new userTasksType(), $tasksobject);
        $labelead = new LeadsLabels();

        $labelform = $this->createForm(new LeadsLabelsType(), $labelead);

        $logObject = new Log();
        $form = $this->createForm(new LogType(), $logObject);

        $leadform = $this->createForm(new LeadsType(), $lead);


        $request = $this->getRequest();

        if ($request->getMethod() == 'POST') {


            $logObject->setUser($user);
            $logObject->setLeads($lead);
            $form->bind($request);
            $logObject->setLeads($lead);

            if ($form->isValid()) {

                $em->persist($logObject);
                $em->flush();
                $logObject = new Log();
                $form = $this->createForm(new LogType(), $logObject);
                $this->get('session')->getFlashBag()->add('success', 'Call Log Added.');
            } else {

                $this->get('session')->getFlashBag()->add('error', 'Call Log Failed.');
            }
        }
        $tasks = $em->getRepository('CrmBundle:Tasks')->findBy(array('lead' => $lead, 'active' => 1), array('notify' => 'ASC'));
        $log = $em->getRepository('CrmBundle:Log')->findBy(array('leads' => $lead), array('created' => 'DESC'));
        $users = $em->getRepository('CrmBundle:User')->findAll();
        $leadusers = $em->getRepository('CrmBundle:LeadsUsers')->findBy(array('lead' => $lead));
        return $this->render('CrmBundle:Leads:history.html.twig', array('usermain'=>$user,'lead' => $lead, 'leadusers' => $leadusers, 'logs' => $log, 'tasks' => $tasks, 'users' => $users, 'form' => $form->createView(), 'leadform' => $leadform->createView(), 'taskform' => $tasksform->createView(), 'formlabel' => $labelform->createView()));
    }

    public function checktaskAction(Tasks $task,Leads $lead) {

        $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();
     $all = $em->getRepository('CrmBundle:Tasks')->findAll();

        foreach($all as $one)
        {
            if($task == $one)
            {
                break;
            }
        }

      //  $task = $em->getRepository('CrmBundle:Tasks')->find($id);

        $task->setActive(0);
        $em->persist($one);
        $em->flush();

        return $this->redirect($this->generateUrl('crm_history', array('lead' => $lead->getId())));
    }

    public function tasksleadAction(Leads $lead) {

        $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();
        $taskobject = new Tasks();
        $form = $this->createForm(new userTasksType(), $taskobject);

        $request = $this->getRequest();

        if ($request->getMethod() == 'POST') {
            $user = $em->getRepository('CrmBundle:User')->find($user);
            $taskobject->setLead($lead);
            $taskobject->setUser($user);
            $form->bind($request);

            if ($form->isValid()) {

                $em->persist($taskobject);
                $em->flush();

                $this->get('session')->getFlashBag()->add('success', 'New Task Added.');
            } else {
                $this->get('session')->getFlashBag()->add('error', 'Failed to add task.');
            }
        }

        return $this->redirect($this->generateUrl('crm_history', array('lead' => $lead->getId())));
    }

    public function addleaduserAction(Request $request,Leads $lead) {

        $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();
        $userobject = $em->getRepository('CrmBundle:User')->find($request->get('user'));
      //  $leadobject = $em->getRepository('CrmBundle:Leads')->find($id);

        $leaduser = new LeadsUsers();
        $leaduser->setUser($userobject);
        $leaduser->setLead($lead);
        $em->persist($leaduser);
        $em->flush();

       $this->get('session')->getFlashBag()->add('success', 'Shared with user.');
        return $this->redirect($this->generateUrl('crm_history', array('lead' => $lead->getId())));
    }
    
    
     public function removeleaduserAction(LeadsUsers $user,Leads $lead) {

        $thisuser = $this->container->get('security.context')->getToken()->getUser();
        if ($thisuser === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();
         $all = $em->getRepository('CrmBundle:Users')->findAll();

        foreach($all as $one)
        {
            if($user == $one)
            {
                break;
            }
        }
         $em->remove($one);
        $em->flush();

       $this->get('session')->getFlashBag()->add('success', 'User removed from lead.');
       
        return $this->redirect($this->generateUrl('crm_history', array('lead' => $lead->getId())));
    }
    


}
