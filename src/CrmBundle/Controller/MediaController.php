<?php

namespace CrmBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CrmBundle\Form\Type\MediaType;
use CrmBundle\Entity\Media;

class MediaController extends Controller
{
    public function indexAction()
    {
          $user = $this->container->get('security.context')->getToken()->getUser();
        if($user === "anon." )
       {
           return $this->redirect($this->generateUrl('fos_user_security_login'));
       }
        
        
            $em = $this->getDoctrine()->getManager();
     
        $leadobject = new Media();
       
        $form = $this->createForm(new MediaType(), $leadobject);

           $request = $this->getRequest();
        
            if ($request->getMethod() == 'POST') {

            $form->bind($request);
            if ($form->isValid()) {
            $em->persist($leadobject);
            $em->flush();
                   $leadobject = new Media();
       
        $form = $this->createForm(new MediaType(), $leadobject);     
    $this->get('session')->getFlashBag()->add('success', 'Media row has been added.');
            }
        }
          

         return $this->redirect($this->generateUrl('crm_campaign'));
        //   return $this->render('CrmBundle:Media:index.html.twig', array('medias' => $leads, 'form' => $form->createView()));
    }
    
    public function deleteAction(Media $media) {
        
          $user = $this->container->get('security.context')->getToken()->getUser();
        if($user === "anon." )
       {
           return $this->redirect($this->generateUrl('fos_user_security_login'));
       }
        

           try {
         
        $em = $this->getDoctrine()->getManager();
            $all = $em->getRepository('CrmBundle:Media')->findAll();

        foreach($all as $one)
        {
            if($media == $one)
            {
                break;
            }
        }
        $em->remove($one);
        $em->flush();
         $this->get('session')->getFlashBag()->add('success', 'Media has been Deleted.');
    }
    catch (\Exception $e) {
             $this->get('session')->getFlashBag()->add('error', 'There are Campaign/Leads working with this media. as an alternative update media name if you should.');
    }
            
        return $this->redirect($this->generateUrl('crm_campaign'));
    }

    public function editAction(Media $media) {
        
          $user = $this->container->get('security.context')->getToken()->getUser();
        if($user === "anon." )
       {
           return $this->redirect($this->generateUrl('fos_user_security_login'));
       }
        
        
        $em = $this->getDoctrine()->getManager();
     
                $form = $this->createForm(new MediaType(), $media);

           $request = $this->getRequest();
        
            if ($request->getMethod() == 'POST') {

            $form->bind($request);
            if ($form->isValid()) {
            $em->persist($media);
            $em->flush();

     $this->get('session')->getFlashBag()->add('success', 'Changes Saved.');
            }
        }

               return $this->render('CrmBundle:Media:editmedia.html.twig', array('form' => $form->createView(),'media' => $media));
    }

}
