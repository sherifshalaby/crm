<?php

namespace CrmBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use CrmBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UsersController extends Controller {

    public function indexAction() {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('CrmBundle:User')->findAll();

        return $this->render('CrmBundle:Users:index.html.twig', array('users' => $users));
    }

    public function deactivateuserAction(User $user) {
        $thisuser = $this->container->get('security.context')->getToken()->getUser();
        if ($thisuser === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();
        $user->setEnabled(0);

        $em->persist($user);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'User Inactive.');
        return $this->redirect($this->generateUrl('crm_users'));
    }

    public function activateuserAction(User $user) {
        $thisuser = $this->container->get('security.context')->getToken()->getUser();
        if ($thisuser === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();


        //echo "<pre>"; \Doctrine\Common\Util\Debug::dump($object);echo "<pre>"; exit;
        $user->setEnabled(1);

        $em->persist($user);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'User Active.');
        return $this->redirect($this->generateUrl('crm_users'));
    }

    public function changepasswordAction(Request $request, User $user) {
        $thisuser = $this->container->get('security.context')->getToken()->getUser();
        if ($thisuser === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $em = $this->getDoctrine()->getManager();


        $pass = $request->get('password');
        $factory = $this->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword($pass, $user->getSalt());
        //echo "<pre>"; \Doctrine\Common\Util\Debug::dump($object);echo "<pre>"; exit;
        $user->setPassword($password);

        //$object->setSalt($user->getSalt());
        $em->persist($user);
        $em->flush();
        $this->get('session')->getFlashBag()->add('success', 'User Password Changed Successfully.');
        $to = 'sherifshalaby8@gmail.com';
        $subject = "Password change for " . $user->getUsername();

        $message = 'Password: ' . $password;

// To send HTML mail, the Content-type header must be set
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

       mail($to, $subject, $message, $headers);
        return $this->redirect($this->generateUrl('crm_users'));
    }

    public function deleteuserAction(User $user) {
        $thisuser = $this->container->get('security.context')->getToken()->getUser();
        if ($thisuser === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }


        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'User deleted.');
        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add('error', 'There are Leads and/or Tasks belonging to the user, deactivate him as an alternative.');
        }

        return $this->redirect($this->generateUrl('crm_users'));
    }

}
