<?php

namespace CrmBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CrmBundle\Form\Type\LogType;
use CrmBundle\Entity\Log;


class LogController extends Controller {

    public function indexAction() {

        $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
 

        $em = $this->getDoctrine()->getManager();

        $logObject = new Log();

        $form = $this->createForm(new LogType(), $logObject);

        $request = $this->getRequest();
 
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            if ($form->isValid()) {
                $logObject->setUser($user);
                $em->persist($logObject);
                $em->flush();
                $logObject = new Log();

                $form = $this->createForm(new LogType(), $logObject);
                $this->get('session')->getFlashBag()->add('success', 'Call Log Added.');
            }
        }
   
   
            $logs = $em->getRepository('CrmBundle:Log')->findBy(array('user' => $user), array('created' => 'DESC'));
            $tasks = $em->getRepository('CrmBundle:Tasks')->findBy(array('user' => $user), array('notify' => 'ASC'));
           
  

        return $this->render('CrmBundle:Log:index.html.twig', array('logs' => $logs, 'tasks' => $tasks, 'form' => $form->createView()));
    }
    
    
    
        public function alllogAction() {

        $thisuser = $this->container->get('security.context')->getToken()->getUser();
        if ($thisuser === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
 

        $em = $this->getDoctrine()->getManager();

        $logObject = new Log();

        $form = $this->createForm(new LogType(), $logObject);

        $request = $this->getRequest();
 
        if ($request->getMethod() == 'POST') {

            $form->bind($request);
            if ($form->isValid()) {
                $logObject->setUser($user);
                $em->persist($logObject);
                $em->flush();
                $logObject = new Log();

                $form = $this->createForm(new LogType(), $logObject);
                $this->get('session')->getFlashBag()->add('success', 'Call Log Added.');
            }
        }
   
     
        
            $logs = $em->getRepository('CrmBundle:Log')->findBy(array(),array('created' => 'DESC'));
            
            $tasks = $em->getRepository('CrmBundle:Tasks')->findBy(array(), array('notify' => 'ASC'));

  
//echo "<pre>";Debug::dump($logs[3]->getUser());exit;

        return $this->render('CrmBundle:Log:alllog.html.twig', array('logs' => $logs, 'tasks' => $tasks, 'form' => $form->createView()));
    }

}
