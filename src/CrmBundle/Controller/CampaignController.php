<?php

namespace CrmBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CrmBundle\Form\Type\CampaignType;
use CrmBundle\Entity\Campaign;
use CrmBundle\Form\Type\MediaType;
use CrmBundle\Entity\Media;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class CampaignController extends Controller {

    public function indexAction() {
        
          $user = $this->container->get('security.context')->getToken()->getUser();
        if($user === "anon." )
       {
           return $this->redirect($this->generateUrl('fos_user_security_login'));
       }
        
        $em = $this->getDoctrine()->getManager();
     
        $campaignobject = new Campaign();
        $form = $this->createForm(new CampaignType(), $campaignobject);
         $mediaobject = new Media();
         $mediaform = $this->createForm(new MediaType(), $mediaobject);
        $request = $this->getRequest();
        
            if ($request->getMethod() == 'POST') {

            $form->bind($request);
            if ($form->isValid()) {
            $em->persist($campaignobject);
            $em->flush();
                   $campaignobject = new Campaign();
       
        $form = $this->createForm(new CampaignType(), $campaignobject);     
      $this->get('session')->getFlashBag()->add('success', 'New Campaign has been created.');
            }
        }
           $campaigns = $em->getRepository('CrmBundle:Campaign')->findAll();
              $media = $em->getRepository('CrmBundle:Media')->findAll();
          
             return $this->render('CrmBundle:Campaign:index.html.twig', array('campaigns' => $campaigns,'media' => $media, 'form' => $form->createView(), 'mediaform' => $mediaform->createView()));
    }

    public function deleteAction(Campaign $campaign) {
  
       // echo "<pre>"; \Doctrine\Common\Util\Debug::dump($post);   exit;
          $user = $this->container->get('security.context')->getToken()->getUser();
        if($user === "anon." )
       {
           return $this->redirect($this->generateUrl('fos_user_security_login'));
       }
        
         $em = $this->getDoctrine()->getManager();
    $all = $em->getRepository('CrmBundle:Campaign')->findAll();

        foreach($all as $one)
        {
            if($campaign == $one)
            {
                break;
            }
        }
                 try {
   
      //  $object = $em->getRepository('CrmBundle:Campaign')->find($id);
        $em->remove($one);
        $em->flush();
     $this->get('session')->getFlashBag()->add('success', 'Campaign Deleted.');
    }
    catch (\Exception $e) {
             $this->get('session')->getFlashBag()->add('error', 'There are Leads collected through this Campaign. as an alternative update Campaign name if you should.');
    }
       
        return $this->redirect($this->generateUrl('crm_campaign'));
    }

    public function editAction(Campaign $campaign) {
        
          $user = $this->container->get('security.context')->getToken()->getUser();
        if($user === "anon." )
       {
           return $this->redirect($this->generateUrl('fos_user_security_login'));
       }
        
        $em = $this->getDoctrine()->getManager();
    //    $labelobject = $em->getRepository('CrmBundle:Campaign')->find($id);
        
                $form = $this->createForm(new CampaignType(), $campaign);

           $request = $this->getRequest();
        
           
           
            if ($request->getMethod() == 'POST') {

            $form->bind($request);
            if ($form->isValid()) {
            $em->persist($campaign);
            $em->flush();

      $this->get('session')->getFlashBag()->add('success', 'Campaign Changes Saved.');
            }
        }

               return $this->render('CrmBundle:Campaign:editcampaign.html.twig', array('form' => $form->createView(),'campaign' => $campaign));
    }

    
}
