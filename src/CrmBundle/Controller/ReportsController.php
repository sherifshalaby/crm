<?php

namespace CrmBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class ReportsController extends Controller {

    public function usersreportAction() {

        $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('CrmBundle:User')->findAll();
         $medias = $em->getRepository('CrmBundle:Media')->findAll();
// echo "<pre>";Debug::dump($medias);exit;
        return $this->render('CrmBundle:Reports:usersreport.html.twig', array('users' => $users, 'medias' => $medias,));
    }

    
        public function medaireportAction() {

        $user = $this->container->get('security.context')->getToken()->getUser();
        if ($user === "anon.") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }
        $em = $this->getDoctrine()->getManager();



        $campaigns = $em->getRepository('CrmBundle:Campaign')->findAll();
        
        $medias = $em->getRepository('CrmBundle:Media')->findAll();
  
// echo "<pre>";Debug::dump($medias);exit;
        return $this->render('CrmBundle:Reports:mediareport.html.twig', array( 'medias' => $medias, 'campaigns' => $campaigns));
    }



}
