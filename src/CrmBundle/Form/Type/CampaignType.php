<?php


namespace CrmBundle\Form\Type;



use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class CampaignType extends AbstractType {
    
   public function buildForm(FormBuilderInterface $builder, array $options)
    {
         $builder
           
          
            ->add('Name','text',array('required'=> true ,'attr' => array('class' => 'col-md-12 form-control input-circle')))
         ->add('Date','date',array('required'=> true ,'attr' => array('class' => 'col-md-12 form-control')))
          
                 ->add('Active','checkbox',array('required'=> false ,'attr' => array('class' => 'col-md-12 form-control input-circle')))
             ->add('Description','textarea',array('required'=> false ,'attr' => array('class' => 'col-md-12 form-control input-circle')))
                    
                 ->add('Media','entity' ,array( 'class' => 'CrmBundle\Entity\Media',
                  'data' => 'Select Media',
                'property' => 'name',
                'multiple' => false,
                'expanded' => false,
                 'attr' => array('class' => 'select2_category form-control','data-placeholder' => 'Choose Media','tabindex' => '1'),
             ));
           
     
    }
     public function setDefaultOptions(OptionsResolverInterface $r)
	{
		$r->setDefaults(array(
			'data_class' => 'CrmBundle\Entity\Campaign'
		));
	}
    public function getName()
    {
        return 'Campaign';
    }  
}
