<?php


namespace CrmBundle\Form\Type;



use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class ActivityType extends AbstractType {
    
   public function buildForm(FormBuilderInterface $builder, array $options)
    {
         $builder
           
     
           
             ->add('Description','textarea',array('required'=> true ,'attr' => array('class' => 'col-md-12 form-control input-circle')));
           
     
    }
     public function setDefaultOptions(OptionsResolverInterface $r)
	{
		$r->setDefaults(array(
			'data_class' => 'CrmBundle\Entity\Activity'
		));
	}
    public function getName()
    {
        return 'Activity';
    }  
}
