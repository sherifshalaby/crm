<?php
namespace CrmBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class LogType extends AbstractType {
    
   public function buildForm(FormBuilderInterface $builder, array $options)
    {
         $builder
   
       
             ->add('Notes','textarea',array('required'=> true ,'attr' => array('class' => 'col-md-12 form-control input-circle')))
             
      ->add('Calltype','choice',array('choices'=>array('InComing Call'=>'InComing Call','OutGoing Call'=>'OutGoing Call','Meeting'=>'Meeting'),'required'=> true ,'attr' => array('class' => 'col-md-12 form-control input-circle')))
           ->add('Status','choice',array('choices'=>array('Pitch'=>'Pitch','Followup'=>'Followup','Reply'=>'Reply'),'required'=> true ,'attr' => array('class' => 'col-md-12 form-control input-circle')))
   
              
             ->add('Leads','entity' ,array( 'class' => 'CrmBundle\Entity\Leads',
                'property' => 'name',
                'multiple' => false,
                'expanded' => false,
                    'attr' => array('class' => 'select2_category form-control','data-placeholder' => 'Choose Media','tabindex' => '1'),
          ));
           
     
    }
     public function setDefaultOptions(OptionsResolverInterface $r)
	{
		$r->setDefaults(array(
			'data_class' => 'CrmBundle\Entity\Log'
		));
	}
    public function getName()
    {
        return 'Log';
    }  
}
