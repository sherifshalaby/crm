<?php


namespace CrmBundle\Form\Type;



use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class LeadsLabelsType extends AbstractType {
    
   public function buildForm(FormBuilderInterface $builder, array $options)
    {
         $builder
           
     
              ->add('Label','entity' ,array( 'class' => 'CrmBundle\Entity\Label',
                'property' => 'name',
                'multiple' => false,
                'expanded' => false,
               'attr' => array('class' => 'select2_category form-control','data-placeholder' => 'Choose Unit','tabindex' => '1')
             ))
           
       ->add('Value', 'textarea', array('required' => false, 'attr' => array('class' => 'col-md-12 form-control input-circle')));
             
    }
     public function setDefaultOptions(OptionsResolverInterface $r)
	{
		$r->setDefaults(array(
			'data_class' => 'CrmBundle\Entity\LeadsLabels'
		));
	}
    public function getName()
    {
        return 'LeadsLabels';
    }  
}
