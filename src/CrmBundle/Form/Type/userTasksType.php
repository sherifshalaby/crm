<?php

namespace CrmBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class userTasksType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('Priority', 'choice', array('choices' => array('High' => 'High', 'Medium' => 'Medium', 'Low' => 'Low'), 'required' => true, 'attr' => array('class' => 'col-md-12 form-control input-circle')))
            
                ->add('Active', 'checkbox', array('required' => false, 'attr' => array('class' => 'col-md-12 form-control input-circle','checked'   => 'checked')))
       
                ->add('Todo', 'textarea', array('required' => true, 'attr' => array('class' => 'col-md-12 form-control input-circle')))
                ->add('Notify', 'text', array(
                     'attr' => array('class' => 'form-control','readonly','size'=>'16','type'=>'text'),
                    'required' => true));
    }

    public function setDefaultOptions(OptionsResolverInterface $r) {
        $r->setDefaults(array(
            'data_class' => 'CrmBundle\Entity\Tasks'
        ));
    }

    public function getName() {
        return 'Tasks';
    }

}
