<?php

namespace CrmBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LeadsType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('Name', 'text', array('required' => true, 'attr' => array('class' => 'col-md-12 form-control input-circle')))
                ->add('Number', 'text', array('required' => false, 'attr' => array('class' => 'col-md-12 form-control input-circle')))
                ->add('Status', 'choice', array('choices' => array('Lead' => 'Lead', 'Not Interested' => 'Not Interested', 'Interested' => 'Interested', 'Converted' => 'Converted', 'Do not contact' => 'Do not contact'), 'required' => true, 'attr' => array('class' => 'col-md-12 form-control input-circle')))
                ->add('Campaign', 'entity', array('class' => 'CrmBundle\Entity\Campaign',
                    'property' => 'name',
                    'multiple' => false,
                    'expanded' => false,
                    'required' => true,
                    'attr' => array('class' => 'select2_category form-control', 'data-placeholder' => 'Choose Campaign', 'tabindex' => '1'),
                ))
                
                     ->add('Media', 'entity', array('class' => 'CrmBundle\Entity\Media',
                    'property' => 'name',
                    'multiple' => false,
                    'expanded' => false,
                     'required' => true,
                    'attr' => array('class' => 'select2_category form-control', 'data-placeholder' => 'Choose Media', 'tabindex' => '1'),
                ))
                ->add('Organization', 'text', array('required' => false,'attr' => array('class' => 'col-md-12 form-control input-circle')))
                ->add('Notes', 'textarea', array('required' => false,'attr' => array('class' => 'col-md-12 form-control input-circle')))
                ->add('Email', 'email', array( 'required' => false,'attr' => array('class' => 'col-md-12 form-control input-circle')))
                ->add('Gender', 'choice', array('choices' => array('Male' => 'Male', 'Female' => 'Female'), 'required' => true, 'attr' => array('class' => 'col-md-12 form-control input-circle')));
    }

    public function setDefaultOptions(OptionsResolverInterface $r) {
        $r->setDefaults(array(
            'data_class' => 'CrmBundle\Entity\Leads'
        ));
    }

    public function getName() {
        return 'Leads';
    }

}
