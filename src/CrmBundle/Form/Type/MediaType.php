<?php


namespace CrmBundle\Form\Type;



use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class MediaType extends AbstractType {
    
   public function buildForm(FormBuilderInterface $builder, array $options)
    {
         $builder
           
          
            ->add('Name','text',array('required'=> true ,'attr' => array('class' => 'col-md-12 form-control')))
    ;
           
     
    }
     public function setDefaultOptions(OptionsResolverInterface $r)
	{
		$r->setDefaults(array(
			'data_class' => 'CrmBundle\Entity\Media'
		));
	}
    public function getName()
    {
        return 'Media';
    }  
}
