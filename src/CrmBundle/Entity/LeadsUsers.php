<?php
namespace CrmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CrmBundle\Entity\Repository\LeadsUsers")
 * @ORM\Table(name="LeadsUsers")
 * @ORM\HasLifecycleCallbacks
 */
class LeadsUsers
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    
    /**
     * @ORM\ManyToOne(targetEntity="CrmBundle\Entity\Leads", inversedBy="leadsusers")
     * @ORM\JoinColumn(name="lead_id", referencedColumnName="id")
     */
    private $lead;
    /**
     * @ORM\ManyToOne(targetEntity="CrmBundle\Entity\User", inversedBy="leadsusers")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

            
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;

        /**
     * Constructor
     */
    public function __construct()
    {

           $this->setCreated(new \DateTime());
        $this->setUpdated(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return LeadsUsers
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return LeadsUsers
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set lead
     *
     * @param \CrmBundle\Entity\Leads $lead
     * @return LeadsUsers
     */
    public function setLead(\CrmBundle\Entity\Leads $lead = null)
    {
        $this->lead = $lead;

        return $this;
    }

    /**
     * Get lead
     *
     * @return \CrmBundle\Entity\Leads 
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * Set user
     *
     * @param \CrmBundle\Entity\User $user
     * @return LeadsUsers
     */
    public function setUser(\CrmBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CrmBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
