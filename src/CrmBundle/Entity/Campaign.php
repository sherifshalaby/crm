<?php
namespace CrmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CrmBundle\Entity\Repository\CampaignRepository")
 * @ORM\Table(name="Campaign")
 * @ORM\HasLifecycleCallbacks
 */
class Campaign
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;
    
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;
    
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $active;
    
    
      /**
     * @ORM\Column(type="date")
     */
    protected $date; 

    /**
     * @ORM\ManyToOne(targetEntity="CrmBundle\Entity\Media", inversedBy="campaign")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     */
    private $media;
    
             /**
     * @ORM\OneToMany(targetEntity="CrmBundle\Entity\Leads", mappedBy="campaign")
     */
    private $lead;
    
            /**
     * @ORM\OneToMany(targetEntity="CrmBundle\Entity\Log", mappedBy="campaign")
     */
    private $log;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Campaign
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Campaign
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Campaign
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Campaign
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set media
     *
     * @param \CrmBundle\Entity\Media $media
     * @return Campaign
     */
    public function setMedia(\CrmBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \CrmBundle\Entity\Media 
     */
    public function getMedia()
    {
        return $this->media;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->log = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add log
     *
     * @param \CrmBundle\Entity\Log $log
     * @return Campaign
     */
    public function addLog(\CrmBundle\Entity\Log $log)
    {
        $this->log[] = $log;

        return $this;
    }

    /**
     * Remove log
     *
     * @param \CrmBundle\Entity\Log $log
     */
    public function removeLog(\CrmBundle\Entity\Log $log)
    {
        $this->log->removeElement($log);
    }

    /**
     * Get log
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * Add lead
     *
     * @param \CrmBundle\Entity\Leads $lead
     * @return Campaign
     */
    public function addLead(\CrmBundle\Entity\Leads $lead)
    {
        $this->lead[] = $lead;

        return $this;
    }

    /**
     * Remove lead
     *
     * @param \CrmBundle\Entity\Leads $lead
     */
    public function removeLead(\CrmBundle\Entity\Leads $lead)
    {
        $this->lead->removeElement($lead);
    }

    /**
     * Get lead
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLead()
    {
        return $this->lead;
    }
}
