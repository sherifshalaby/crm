<?php


namespace CrmBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
        /**
     * @ORM\OneToMany(targetEntity="CrmBundle\Entity\Leads", mappedBy="user")
     */
    private $lead;
    
     /**
     * @ORM\OneToMany(targetEntity="CrmBundle\Entity\Log", mappedBy="user")
     */
    private $log;
    
         /**
     * @ORM\OneToMany(targetEntity="CrmBundle\Entity\LeadsUsers", mappedBy="user")
     */
    private $leadsusers;
    
            /**
     * @ORM\OneToMany(targetEntity="CrmBundle\Entity\Tasks", mappedBy="user")
     */
    private $tasks;
    
         /**
     * @ORM\Column(type="integer",nullable=true)
     */
    protected $privs;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add log
     *
     * @param \CrmBundle\Entity\Log $log
     * @return User
     */
    public function addLog(\CrmBundle\Entity\Log $log)
    {
        $this->log[] = $log;

        return $this;
    }

    /**
     * Remove log
     *
     * @param \CrmBundle\Entity\Log $log
     */
    public function removeLog(\CrmBundle\Entity\Log $log)
    {
        $this->log->removeElement($log);
    }

    /**
     * Get log
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * Add lead
     *
     * @param \CrmBundle\Entity\Leads $lead
     * @return User
     */
    public function addLead(\CrmBundle\Entity\Leads $lead)
    {
        $this->lead[] = $lead;

        return $this;
    }

    /**
     * Remove lead
     *
     * @param \CrmBundle\Entity\Leads $lead
     */
    public function removeLead(\CrmBundle\Entity\Leads $lead)
    {
        $this->lead->removeElement($lead);
    }

    /**
     * Get lead
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * Add tasks
     *
     * @param \CrmBundle\Entity\Tasks $tasks
     * @return User
     */
    public function addTask(\CrmBundle\Entity\Tasks $tasks)
    {
        $this->tasks[] = $tasks;

        return $this;
    }

    /**
     * Remove tasks
     *
     * @param \CrmBundle\Entity\Tasks $tasks
     */
    public function removeTask(\CrmBundle\Entity\Tasks $tasks)
    {
        $this->tasks->removeElement($tasks);
    }

    /**
     * Get tasks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * Set privs
     *
     * @param integer $privs
     * @return User
     */
    public function setPrivs($privs)
    {
        $this->privs = $privs;

        return $this;
    }

    /**
     * Get privs
     *
     * @return integer 
     */
    public function getPrivs()
    {
        return $this->privs;
    }

    /**
     * Add leadsusers
     *
     * @param \CrmBundle\Entity\LeadsUsers $leadsusers
     * @return User
     */
    public function addLeadsuser(\CrmBundle\Entity\LeadsUsers $leadsusers)
    {
        $this->leadsusers[] = $leadsusers;

        return $this;
    }

    /**
     * Remove leadsusers
     *
     * @param \CrmBundle\Entity\LeadsUsers $leadsusers
     */
    public function removeLeadsuser(\CrmBundle\Entity\LeadsUsers $leadsusers)
    {
        $this->leadsusers->removeElement($leadsusers);
    }

    /**
     * Get leadsusers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLeadsusers()
    {
        return $this->leadsusers;
    }
}
