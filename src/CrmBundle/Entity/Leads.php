<?php
namespace CrmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CrmBundle\Entity\Repository\LeadsRepository")
 * @ORM\Table(name="Leads")
 * @ORM\HasLifecycleCallbacks
 */
class Leads
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

        /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $number;
    
    /**
     * @ORM\Column(type="string")
     */
    
       protected $status;
    
    /**
     * @ORM\Column(type="string")
     */
    
    
    protected $name;
    
        /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $email;

        

    
            /**
     * @ORM\Column(type="string")
     */
    protected $gender;
    
             /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $organization;
           /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $notes;
    
        /**
     * @ORM\OneToMany(targetEntity="CrmBundle\Entity\Log", mappedBy="leads")
     */
    private $log;
    
    /**
     * @ORM\OneToMany(targetEntity="CrmBundle\Entity\LeadsLabels", mappedBy="lead")
     */
    private $leadslabels;
    
        /**
     * @ORM\OneToMany(targetEntity="CrmBundle\Entity\Tasks", mappedBy="lead")
     */
    private $tasks;
    
            /**
     * @ORM\OneToMany(targetEntity="CrmBundle\Entity\LeadsUsers", mappedBy="lead")
     */
    private $leadsusers;

                /**
     * @ORM\ManyToOne(targetEntity="CrmBundle\Entity\User", inversedBy="lead")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
                    /**
     * @ORM\ManyToOne(targetEntity="CrmBundle\Entity\Campaign", inversedBy="lead")
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    private $campaign;
    
                        /**
     * @ORM\ManyToOne(targetEntity="CrmBundle\Entity\Media", inversedBy="lead")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     */
    private $media;
        
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;


    /**
     * Constructor
     */
    public function __construct()
    {
         $this->setCreated(new \DateTime());
        $this->setUpdated(new \DateTime());
        $this->log = new \Doctrine\Common\Collections\ArrayCollection();
        $this->leadslabels = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Leads
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Leads
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Leads
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Leads
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set gender
     *
     * @param string $gender
     * @return Leads
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string 
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Add log
     *
     * @param \CrmBundle\Entity\Log $log
     * @return Leads
     */
    public function addLog(\CrmBundle\Entity\Log $log)
    {
        $this->log[] = $log;

        return $this;
    }

    /**
     * Remove log
     *
     * @param \CrmBundle\Entity\Log $log
     */
    public function removeLog(\CrmBundle\Entity\Log $log)
    {
        $this->log->removeElement($log);
    }

    /**
     * Get log
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * Add leadslabels
     *
     * @param \CrmBundle\Entity\LeadsLabels $leadslabels
     * @return Leads
     */
    public function addLeadslabel(\CrmBundle\Entity\LeadsLabels $leadslabels)
    {
        $this->leadslabels[] = $leadslabels;

        return $this;
    }

    /**
     * Remove leadslabels
     *
     * @param \CrmBundle\Entity\LeadsLabels $leadslabels
     */
    public function removeLeadslabel(\CrmBundle\Entity\LeadsLabels $leadslabels)
    {
        $this->leadslabels->removeElement($leadslabels);
    }

    /**
     * Get leadslabels
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLeadslabels()
    {
        return $this->leadslabels;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Leads
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set iscustomer
     *
     * @param boolean $iscustomer
     * @return Leads
     */
    public function setIscustomer($iscustomer)
    {
        $this->iscustomer = $iscustomer;

        return $this;
    }

    /**
     * Get iscustomer
     *
     * @return boolean 
     */
    public function getIscustomer()
    {
        return $this->iscustomer;
    }

    /**
     * Set organization
     *
     * @param string $organization
     * @return Leads
     */
    public function setOrganization($organization)
    {
        $this->organization = $organization;

        return $this;
    }

    /**
     * Get organization
     *
     * @return string 
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return Leads
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Leads
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Leads
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set user
     *
     * @param \CrmBundle\Entity\User $user
     * @return Leads
     */
    public function setUser(\CrmBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CrmBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set campaign
     *
     * @param \CrmBundle\Entity\Campaign $campaign
     * @return Leads
     */
    public function setCampaign(\CrmBundle\Entity\Campaign $campaign = null)
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * Get campaign
     *
     * @return \CrmBundle\Entity\Campaign 
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * Set media
     *
     * @param \CrmBundle\Entity\Media $media
     * @return Leads
     */
    public function setMedia(\CrmBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \CrmBundle\Entity\Media 
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Add tasks
     *
     * @param \CrmBundle\Entity\Tasks $tasks
     * @return Leads
     */
    public function addTask(\CrmBundle\Entity\Tasks $tasks)
    {
        $this->tasks[] = $tasks;

        return $this;
    }

    /**
     * Remove tasks
     *
     * @param \CrmBundle\Entity\Tasks $tasks
     */
    public function removeTask(\CrmBundle\Entity\Tasks $tasks)
    {
        $this->tasks->removeElement($tasks);
    }

    /**
     * Get tasks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * Add leadsusers
     *
     * @param \CrmBundle\Entity\LeadsUsers $leadsusers
     * @return Leads
     */
    public function addLeadsuser(\CrmBundle\Entity\LeadsUsers $leadsusers)
    {
        $this->leadsusers[] = $leadsusers;

        return $this;
    }

    /**
     * Remove leadsusers
     *
     * @param \CrmBundle\Entity\LeadsUsers $leadsusers
     */
    public function removeLeadsuser(\CrmBundle\Entity\LeadsUsers $leadsusers)
    {
        $this->leadsusers->removeElement($leadsusers);
    }

    /**
     * Get leadsusers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLeadsusers()
    {
        return $this->leadsusers;
    }
}
