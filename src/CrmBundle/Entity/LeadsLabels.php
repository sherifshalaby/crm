<?php
namespace CrmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CrmBundle\Entity\Repository\LeadsLabels")
 * @ORM\Table(name="LeadsLabels")
 * @ORM\HasLifecycleCallbacks
 */
class LeadsLabels
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $value;

    
    /**
     * @ORM\ManyToOne(targetEntity="CrmBundle\Entity\Leads", inversedBy="leadslabels")
     * @ORM\JoinColumn(name="lead_id", referencedColumnName="id")
     */
    private $lead;
    /**
     * @ORM\ManyToOne(targetEntity="CrmBundle\Entity\Label", inversedBy="leadslabels")
     * @ORM\JoinColumn(name="label_id", referencedColumnName="id")
     */
    private $label;

            
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;

        /**
     * Constructor
     */
    public function __construct()
    {

           $this->setCreated(new \DateTime());
        $this->setUpdated(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return LeadsLabels
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set lead
     *
     * @param \CrmBundle\Entity\Leads $lead
     * @return LeadsLabels
     */
    public function setLead(\CrmBundle\Entity\Leads $lead = null)
    {
        $this->lead = $lead;

        return $this;
    }

    /**
     * Get lead
     *
     * @return \CrmBundle\Entity\Leads 
     */
    public function getLead()
    {
        return $this->lead;
    }

    /**
     * Set label
     *
     * @param \CrmBundle\Entity\Label $label
     * @return LeadsLabels
     */
    public function setLabel(\CrmBundle\Entity\Label $label = null)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return \CrmBundle\Entity\Label 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return LeadsLabels
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return LeadsLabels
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
