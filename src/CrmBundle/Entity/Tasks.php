<?php

namespace CrmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CrmBundle\Entity\Repository\Tasks")
 * @ORM\Table(name="Tasks")
 * @ORM\HasLifecycleCallbacks
 */
class Tasks {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="text")
     */
    protected $todo;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $active;

    /**
     * @ORM\ManyToOne(targetEntity="CrmBundle\Entity\Leads", inversedBy="tasks")
     * @ORM\JoinColumn(name="lead_id", referencedColumnName="id")
     */
    private $lead;

    /**
     * @ORM\ManyToOne(targetEntity="CrmBundle\Entity\User", inversedBy="tasks")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $notify;

    /**
     * @ORM\Column(type="string")
     */
    protected $priority;
            
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;

        /**
     * Constructor
     */
    public function __construct()
    {

           $this->setCreated(new \DateTime());
        $this->setUpdated(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Tasks
     */
    public function setValue($value) {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Set lead
     *
     * @param \CrmBundle\Entity\Leads $lead
     * @return Tasks
     */
    public function setLead(\CrmBundle\Entity\Leads $lead = null) {
        $this->lead = $lead;

        return $this;
    }

    /**
     * Get lead
     *
     * @return \CrmBundle\Entity\Leads 
     */
    public function getLead() {
        return $this->lead;
    }

    /**
     * Set user
     *
     * @param \CrmBundle\Entity\User $user
     * @return Tasks
     */
    public function setUser(\CrmBundle\Entity\User $user = null) {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CrmBundle\Entity\User 
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * Set todo
     *
     * @param string $todo
     * @return Tasks
     */
    public function setTodo($todo) {
        $this->todo = $todo;

        return $this;
    }

    /**
     * Get todo
     *
     * @return string 
     */
    public function getTodo() {
        return $this->todo;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Tasks
     */
    public function setActive($active) {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive() {
        return $this->active;
    }

    /**
     * Set notify
     *
     * @param \DateTime $notify
     * @return Tasks
     */
    public function setNotify($notify) {

        //yyyy-MM-dd HH:i

        $notify = explode(' ', $notify);
        $month = date("m", strtotime($notify[1]));
        $year = $notify[2];
        $day = $notify[0];
        $time = explode(':', $notify[4]);
        $minute = $time[1];
        $hour = $time[0];
        $notify = $year . '-' . $month . '-' . $day . ' ' . $hour . ':' . $minute . ':00';

        $this->notify = new \DateTime($notify);

        return $this;
    }

    /**
     * Get notify
     *
     * @return \DateTime 
     */
    public function getNotify() {
        return $this->notify;
    }

    /**
     * Set priority
     *
     * @param string $priority
     * @return Tasks
     */
    public function setPriority($priority) {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return string 
     */
    public function getPriority() {
        return $this->priority;
    }


    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Tasks
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Tasks
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }
}
