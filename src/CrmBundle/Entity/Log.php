<?php
namespace CrmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CrmBundle\Entity\Repository\LogRepository")
 * @ORM\Table(name="Log")
 * @ORM\HasLifecycleCallbacks
 */
class Log
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\ManyToOne(targetEntity="CrmBundle\Entity\Leads", inversedBy="log")
     * @ORM\JoinColumn(name="leads_id", referencedColumnName="id")
     */
    private $leads;
    
    
     /**
     * @ORM\Column(type="string")
     */
    protected $calltype;
    
         /**
     * @ORM\Column(type="string")
     */
    protected $status;
    
       /**
     * @ORM\Column(type="text")
     */
    protected $notes;

    
        /**
     * @ORM\ManyToOne(targetEntity="CrmBundle\Entity\User", inversedBy="log")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
        /**
     * @ORM\ManyToOne(targetEntity="CrmBundle\Entity\Campaign", inversedBy="log")
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    private $campaign;
    
        
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $updated;

        /**
     * Constructor
     */
    public function __construct()
    {

           $this->setCreated(new \DateTime());
        $this->setUpdated(new \DateTime());
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set leads
     *
     * @param \CrmBundle\Entity\Leads $leads
     * @return Log
     */
    public function setLeads(\CrmBundle\Entity\Leads $leads = null)
    {
        $this->leads = $leads;

        return $this;
    }

    /**
     * Get leads
     *
     * @return \CrmBundle\Entity\Leads 
     */
    public function getLeads()
    {
        return $this->leads;
    }

    /**
     * Set media
     *
     * @param \CrmBundle\Entity\Media $media
     * @return Log
     */
    public function setMedia(\CrmBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \CrmBundle\Entity\Media 
     */
    public function getMedia()
    {
        return $this->media;
    }

    /**
     * Set campaign
     *
     * @param \CrmBundle\Entity\Campaign $campaign
     * @return Log
     */
    public function setCampaign(\CrmBundle\Entity\Campaign $campaign = null)
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * Get campaign
     *
     * @return \CrmBundle\Entity\Campaign 
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * Set activity
     *
     * @param \CrmBundle\Entity\Activity $activity
     * @return Log
     */
    public function setActivity(\CrmBundle\Entity\Activity $activity = null)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return \CrmBundle\Entity\Activity 
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Log
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Log
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Log
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set user
     *
     * @param \CrmBundle\Entity\User $user
     * @return Log
     */
    public function setUser(\CrmBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CrmBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set calltype
     *
     * @param string $calltype
     * @return Log
     */
    public function setCalltype($calltype)
    {
        $this->calltype = $calltype;

        return $this;
    }

    /**
     * Get calltype
     *
     * @return string 
     */
    public function getCalltype()
    {
        return $this->calltype;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Log
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return Log
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string 
     */
    public function getNotes()
    {
        return $this->notes;
    }
}
