<?php
namespace CrmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CrmBundle\Entity\Repository\MediaRepository")
 * @ORM\Table(name="Media")
 * @ORM\HasLifecycleCallbacks
 */
class Media
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;
    

    /**
     * @ORM\OneToMany(targetEntity="CrmBundle\Entity\Campaign", mappedBy="media")
     */
    private $campaign;


            /**
     * @ORM\OneToMany(targetEntity="CrmBundle\Entity\Leads", mappedBy="media")
     */
    private $lead;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Media
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->campaign = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add campaign
     *
     * @param \CrmBundle\Entity\Campaign $campaign
     * @return Media
     */
    public function addCampaign(\CrmBundle\Entity\Campaign $campaign)
    {
        $this->campaign[] = $campaign;

        return $this;
    }

    /**
     * Remove campaign
     *
     * @param \CrmBundle\Entity\Campaign $campaign
     */
    public function removeCampaign(\CrmBundle\Entity\Campaign $campaign)
    {
        $this->campaign->removeElement($campaign);
    }

    /**
     * Get campaign
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * Add log
     *
     * @param \CrmBundle\Entity\Log $log
     * @return Media
     */
    public function addLog(\CrmBundle\Entity\Log $log)
    {
        $this->log[] = $log;

        return $this;
    }

    /**
     * Remove log
     *
     * @param \CrmBundle\Entity\Log $log
     */
    public function removeLog(\CrmBundle\Entity\Log $log)
    {
        $this->log->removeElement($log);
    }

    /**
     * Get log
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * Add lead
     *
     * @param \CrmBundle\Entity\Leads $lead
     * @return Media
     */
    public function addLead(\CrmBundle\Entity\Leads $lead)
    {
        $this->lead[] = $lead;

        return $this;
    }

    /**
     * Remove lead
     *
     * @param \CrmBundle\Entity\Leads $lead
     */
    public function removeLead(\CrmBundle\Entity\Leads $lead)
    {
        $this->lead->removeElement($lead);
    }

    /**
     * Get lead
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLead()
    {
        return $this->lead;
    }
}
