<?php
namespace CrmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="CrmBundle\Entity\Repository\LabelRepository")
 * @ORM\Table(name="Label")
 * @ORM\HasLifecycleCallbacks
 */
class Label
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;
    
        /**
     * @ORM\Column(type="string")
     */
    protected $type;
    
    
    /**
     * @ORM\OneToMany(targetEntity="CrmBundle\Entity\LeadsLabels", mappedBy="label")
     */
    private $leadslabels;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->leadslabels = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Label
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name." ".$this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Label
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add leadslabels
     *
     * @param \CrmBundle\Entity\LeadsLabels $leadslabels
     * @return Label
     */
    public function addLeadslabel(\CrmBundle\Entity\LeadsLabels $leadslabels)
    {
        $this->leadslabels[] = $leadslabels;

        return $this;
    }

    /**
     * Remove leadslabels
     *
     * @param \CrmBundle\Entity\LeadsLabels $leadslabels
     */
    public function removeLeadslabel(\CrmBundle\Entity\LeadsLabels $leadslabels)
    {
        $this->leadslabels->removeElement($leadslabels);
    }

    /**
     * Get leadslabels
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLeadslabels()
    {
        return $this->leadslabels;
    }
}
